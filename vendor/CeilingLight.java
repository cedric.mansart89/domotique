package vendor;

public class CeilingLight {

    public void on() {
        System.out.println("CeilingLight On");
    }

    public void off() {
        System.out.println("CeilingLight Off");
    }
}