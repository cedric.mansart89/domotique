package alexa;

public class SimpleRemoteControl {

    private AlexaCommand alexaCommand;

    public SimpleRemoteControl(AlexaCommand alexaCommand) {
        this.alexaCommand = alexaCommand;
    }

    public void pressButton(){
        alexaCommand.execute();
    }
}
