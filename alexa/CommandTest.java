package alexa;

import vendor.CeilingLight;

public class CommandTest {
    public static void main(String[] args) {
        CeilingLight ceilingLight = new CeilingLight();
        
        AlexaCommand alexaCommand = new CeilingLightOnCmd(ceilingLight);

        SimpleRemoteControl simpleRemoteControl = new SimpleRemoteControl(alexaCommand);
        simpleRemoteControl.pressButton();

    }
}
