package alexa;

public interface AlexaCommand {

    public void execute();
    
}
