package alexa;

import vendor.CeilingLight;


public class CeilingLightOnCmd implements AlexaCommand {
    
    private CeilingLight ceilingLight;

    public CeilingLightOnCmd(CeilingLight ceilingLight){
        this.ceilingLight = ceilingLight;
    }

    @Override
    public void execute() {
        ceilingLight.on();
    }
}
